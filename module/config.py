from __future__ import (
    absolute_import, division, print_function, unicode_literals, with_statement
)
import json


class Config:

    def __init__(self, configFile='/etc/crowd_pam.conf'):
        try:
            with open(configFile, 'r') as f:
                raw = f.read()
                try:
                    config_data = json.loads(raw)
                    # TODO: validate the data
                    for k, v in config_data.iteritems():
                        setattr(self, k, v)

                except Exception as e:
                    raise Exception("Error parsing JSON: {}".format(e))
        except Exception as e:
            raise Exception("Error reading config file: {}".format(e))

    def __del__(self):
        pass
