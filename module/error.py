from __future__ import (
    absolute_import, division, print_function, unicode_literals, with_statement
)


class ERROR:
    """A set of error codes"""
    CROWD_CONNECT_FAILED = 1
    INVALID_USERNAME = 2
    AUTHENTICATION_FAILED = 3
